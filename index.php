<?php include_once( "config.php"); ?>
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <title>Acanac Inc.</title>
    <link rel="icon" type="image/x-icon" href="img/favicon.ico" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/main.css?v=0.0.7">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-clockpicker.min.css">
    <link rel="stylesheet" type="text/css" href="css/tiny-slider.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <!--[if (lt IE 9)]><script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.helper.ie8.js"></script><![endif]-->
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NQGF2PL');
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('config', 'AW-1069262463');
        gtag('config', 'AW-1069262463/JDfaCK_x8GMQ_8zu_QM', {
            'phone_conversion_number': '1-855-235-8886'
        });

    </script>
    <!-- End Google Tag Manager -->
    <style>
        .banner p {
            margin-bottom: 0;
        }

        .freeoffer {
            padding: 2rem;
            background: #fae600;
            border-radius: 25px;
            margin: 10px 0;
            font-weight: bolder;
            color: #0e1f73;
        }

        .freeoffer h1 {
            font: 800 33px "Open Sans", sans-serif;
        }

        .orderbtn {
            margin-bottom: 20px;
            margin-top: 15px;
        }

        .bannerorderbtn {
            margin-bottom: 20px;
            margin-top: 15px;
            margin-left: 50%;
            margin-right: 50%;
        }

        .first-bx a{
        margin-left: 50%;
            margin-right: 50%;
        }
        .cnt-btm {
            font-size: 40px;
        }

        p.small {
            font-size: 2.1rem;
        }

        body {
            background-color: #00B9E4;
        }
        }

    </style>
</head>

<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NQGF2PL" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Add your site or application content here -->
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="logo">
                        <a href="https://www.acanac.com/" title="Acanac Inc."><img src="img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <p><a class="number btn" href="tel:1-855-235-8886"><i class="icon-phone"><img src="img/phone-receiver.svg"></i>1-855-235-8886</a>
                        <a class="number btn" data-toggle="modal" data-target="#popform"><i class="icon-phone"><img src="img/order-now.svg" /></i>Order Online</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="first-bx">
            <div class="banner cf">

                <div class="banner-pic">
                    <img src="img/banner-4.png" alt="">
                </div>
                <div class="banner-txt">
                    <div class="col-sm-2">
                        <h1 style="background: #FAE700">Early</h1>
                    </div>
                    <div class="col-sm-2">
                        <h1>Black</h1>
                    </div>
                    <div class="col-sm-2">
                        <h1>Friday</h1>
                    </div>
                    <div class="col-sm-2">
                        <h1>Sale</h1>
                    </div>
                </div>
                </div>
                <div class="monthfree">
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-5">
                            <h3>6 months free</h3>
                            <h5>Free Installation &amp; Activation</h5>
                        </div>
                        <div class="col-sm-1">
                            <p style="color: #031F73;font-size: 4rem; margin-bottom:0px;">50</p>
                            <p style="color: #031F73;">Mbps</p>
                        </div>
                        <div class="col-sm-2"> <span style="color: #031F73;font-size: 1.6rem;">
                                <p>High</p>
                                <p>Speed</p>
                                <p>Internet</p>
                            </span></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col"></div>
                    <div class="col">
                    <a href="tel:1-855-235-8886" class="btn bannerorderbtn" style="margin-left: 0; margin-right:0;">Call now! Offer ends Nov 30.<br> <i class="icon-phone"><img src="img/phone-receiver.svg"></i>1-855-235-8886 </a></div>
                    <div class="col"></div>
                </div>
            
        </div>
        <br>
        <div class="historybx">
            <div class="row">
                <div class="col">
                    <h2 align="center" style="background: #021f73; color:#ffffff;">
                        <div class="historyinfo">In 2004, we thought "someone must be able to make the internet more affordable". Turns out that "someone" was us, and we've been doing it ever since.</div>
                    </h2>
                </div>
            </div>
        </div>
        <div class="lightning">
            <div class="row">
               <div class="col" style="background:transparent url('img/lightnight-bg.png') no-repeat right center /contain; margin-left:-150px; padding:100px;"></div>
            </div>
        </div>
        <div class="first6months">
            <div class="row">
                <div class="col-md">
                    <hr style="border-top: 5px solid #008cd5; border-bottom: 5px solid #008cd5;">
                </div>
                <div class="col-md">
                    <h2 align="center" style="color:#ffffff;">First 6 months FREE</h2>
                </div>
                <div class="col-md">
                    <hr style="border-top: 5px solid #008cd5; border-bottom: 5px solid #008cd5;">
                </div>
            </div>
        </div>
        <div class="main-wrap cf">
            <div class="padd-bx package-main cf">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="package cf">
                                <div class="row">
                                    <div class="cnt-bx np">
                                        <div class="top-cnt">
                                            <h4>Unlimited</h4>
                                            <h5>50
                                                <span>Mbps</span></h5>
                                            <h3>50 Mbps Download / 10 Mbps Upload</h3>

                                        </div>
                                        <div class="cnt-btm">
                                            <span class="dlspeed">$24.95</span>
                                            <span class="rates">/mo</span><br>
                                            <span class="rates">with 1-year contract<sup>*</sup></span>
                                        </div>
                                    </div>
                                </div>
                                <p></p>
                                <a href="https://www.acanac.com/internet-ontario/fibre/50-10/1-month-best-deal/Unlimited-B/" target='_blank' class="btn orderbtn"><i class="icon-order"><img src="img/order-now.svg" /></i> Order Online</a>
                            </div>

                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="package cf">
                                <div class="row">
                                    <div class="cnt-bx np">
                                        <div class="top-cnt">
                                            <!-- <div class="crown"></div> -->
                                            <!-- <div class="ribbonnew ribbon-top-right"><span>New Price</span></div> -->
                                            <h4>Unlimited</h4>
                                            <h5>25
                                                <span>Mbps</span></h5>
                                            <h3>25 Mbps Download / 10 Mbps Upload</h3>
                                            <!-- <img class="sixmonthfree" src="img/star_free.svg" alt="Get 6 Months Free"> -->
                                        </div>
                                        <div class="cnt-btm">
                                            <span class="dlspeed">$22.95</span>
                                            <span class="rates">/mo</span><br>
                                            <span class="rates">with 1-year contract<sup>*</sup></span>
                                        </div>
                                    </div>
                                </div>
                                <a href="https://www.acanac.com/internet-ontario/fibre/25-10/1-month-best-deal/Unlimited-B/" target='_blank' class="btn orderbtn"><i class="icon-order"><img src="img/order-now.svg"></i> Order Online</a>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <div class="package cf">
                                <div class="row">
                                    <div class="cnt-bx np">
                                        <div class="top-cnt">
                                            <!-- <div class="crownn lt"></div> -->
                                            <!-- <div class="ribbon lt">Limited Time Offer</div> -->
                                            <!-- <div class="ribbonnew ribbon-top-right"><span>Limited Time Offer</span></div> -->
                                            <h4>Unlimited</h4>
                                            <h5>15
                                                <span>Mbps</span></h5>
                                            <h3>15 Mbps Download / 1 Mbps Upload</h3>
                                            <!-- <img class="sixmonthfree" src="img/star_free.svg" alt="Get 6 Months Free"> -->
                                        </div>
                                        <div class="cnt-btm">
                                            <span class="dlspeed">$19.95</span>
                                            <span class="rates">/mo</span><br>
                                            <span class="rates">with 1-year contract<sup>*</sup></span>
                                        </div>
                                    </div>
                                </div>
                                <a href="https://www.acanac.com/internet-ontario/fibre/15-1/1-month-best-deal/Unlimited-B/" target='_blank' class="btn orderbtn"><i class="icon-order"><img src="img/order-now.svg" /></i> Order Online</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="customer">
            <div class="row" style="background:transparent url('img/money-background.png') no-repeat right center /cover">
                <div class="col-2 col-sm-offset-2"><img src="img/customer_satisfaction_badge.png" alt="Customer Satisfaction is our Priority" />
                </div>
                <div class="col">
                    <h3 style="color:#ffffff">
                        Switching is easy, we promise. And if you're not 100% thrilled, just cancel within 30 days. No questions. No hassle. Swearsies.</h3>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer padd-box cf">
        <div class="f-btm cf">
            <div class="container">
                <p>&copy;<?php echo date("Y"); ?> Acanac Inc. All Rights Reserved.</p>
                <p>*You may use your own compatible modem with our internet plans. Free installation and activation. Shipping fees may apply. Promotional prices valid for 12 months. If you are not 100% satisfied with your purchase of Acanac High Speed internet service, you may cancel the service within 30 days of activation. Plans and pricing subject to technical availability. Full terms and conditions are available at <a href="https://www.acanac.com/">Acanac.com</a>.
                </p>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="popform" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <form name="homeform1" id="homeform1" method="post" action="mails/mail.php">
                    <div class="modal-header">
                        <a class="close" data-dismiss="modal" aria-label="Close" title="Close"><span aria-hidden="true">&times;</span></a>
                        <h4 class="modal-title" id="myModalLabel">Request a Call Back</h4>
                    </div>
                    <div class="modal-body">
                        <div class="pop-form">
                            <div class="input-holder">
                                <input type="text" name="cfname" id="cfname" class="required" title="First Name" tabindex="25" placeholder="First Name*
                                ">
                            </div>
                            <div class="input-holder">
                                <input type="text" name="clname" id="clname" class="required" title="Last Name" tabindex="26" placeholder="Last Name*
                                ">
                            </div>

                            <div class="input-holder">
                                <input type="text" name="cemail" id="cemail" class="required email" title="Email" tabindex="28" placeholder="Email*
                                ">
                            </div>

                            <div class="input-holder">
                                <input type="tel" name="cphone" id="cphone" class="required tel" title="Phone" tabindex="27" placeholder="Phone*
                                ">
                            </div>

                            <div class="input-holder">
                                The best time to call*
                                <div class="input-group clockpicker required" data-placement="bottom" data-align="right" data-autoclose="true" title="Time">

                                    <input type="text" class="form-control" value="00:00" name="ctime" id="ctime" tabindex="30">

                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="cap-box cf">

                                <input type="hidden" class="captcharequired" name="captcha_hid_home1" id="captcha_hid_home1" value="">

                                <div class="input-holder">
                                    <span class="errors" id="msghome1"></span>
                                </div>

                                <div class="cont-cap cf" id="captcha_div_home1">
                                    <?php include( 'captcha_code_home_form2.php'); ?>
                                </div>

                                <input type="submit" value="submit" class="btn" tabindex="30" id="btn-submit111" title="Submit">

                                <div id="wait-contactform-contact1" class="wait-contactform-contact1" style="display:none;">
                                    <div class="pls-wait-img-contact"><img src="shapes/wait.gif" align="left" border="0" class="loader-img" style="margin-right:5px; margin-top:3px; margin-left:5px;" /></div>
                                    <div class="pls-wait-text-contact">Please&nbsp;wait...</div>
                                </div>

                            </div>
                        </div>
                    </div>

                </form>





            </div>
        </div>
    </div>
    <!--====== Modal End ======-->

    <script src="js/vendor/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-clockpicker.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/vendor/tiny-slider.js"></script>
    <script src="js/vendor/headroom.min.js"></script>

    <!-- Start of HubSpot Embed Code -->
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/3449027.js"></script> <!-- End of HubSpot Embed Code -->
    <script type="text/javascript">
        $('.clockpicker').clockpicker();

    </script>
    <script type="text/JavaScript">

        jQuery(document).ready(function() {

    jQuery( "#cfname" ).focus(function() {
      refreshhome1();
    });

    jQuery('#btn-submit111').click(function(e){
        //alert("hi");

        jQuery('#btn-submit111').hide();
        jQuery('.wait-contactform-contact1').show();

        // Declare the function variables:
        // Parent form, form URL, email regex and the error HTML
        var $formId = jQuery(this).parents('form');
        var formAction = $formId.attr('action');
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var phoneReg = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
        var $error = jQuery('<span class="error"></span>');
        //alert($formId);
        // Prepare the form for validation - remove previous errors
        //$('li',$formId).removeClass('error');
        jQuery('span.error').remove();
        jQuery('.input-holder').removeClass('error');
        jQuery('#msghome1').hide();
        jQuery('#msghome1').removeClass('newerror');
        jQuery("#msghome1").html("");
        // Validate all inputs with the class "required"
        //alert("test");

        jQuery('.required',$formId).each(function(){

            var inputVal = jQuery.trim(jQuery(this).val());
            var fieldtitle = jQuery(this).attr("title");
            var fieldname=jQuery(this).attr("name");
            var $parentTag = jQuery(this).parent();

            if(inputVal == '' || inputVal=='-1' || $(this).hasClass("clockpicker") ){

                 if($(this).hasClass("clockpicker")){
                    if($(this).find(".form-control").val()=="00:00"){
                         $parentTag.addClass('error').append($error.clone().text('Please select the '+fieldtitle+'.'));
                          jQuery(this).focus();
                          jQuery('#btn-submit111').show();
                          jQuery('.wait-contactform-contact1').hide();
                         return false;
                    }

                 }
                 else{
                $parentTag.addClass('error').append($error.clone().text('Please enter your '+fieldtitle+'.'));
                  jQuery(this).focus();
                  jQuery('#btn-submit111').show();
                  jQuery('.wait-contactform-contact1').hide();
                return false;
                }

            }


            if(jQuery(this).hasClass('email') == true){
                if(!emailReg.test(inputVal)){
                    $parentTag.addClass('error').append($error.clone().text('Sorry, you have entered an invalid Email Address.'));
                    jQuery(this).focus();
                     jQuery('#btn-submit111').show();
                     jQuery('.wait-contactform-contact1').hide();
                    return false;
                }
            }


            if(jQuery(this).hasClass('tel') == true){
                if(!phoneReg.test(inputVal)){
                    $parentTag.addClass('error').append($error.clone().text('Phone number seems invalid.'));
                    jQuery(this).focus();
                     jQuery('#btn-submit111').show();
                     jQuery('.wait-contactform-contact1').hide();
                    return false;
                }
            }

        });


        // All validation complete - Check if any errors exist
        // If has errors
        if (jQuery('span.error').length > 0) {

            jQuery('span.error').each(function(){

                // Set the distance for the error animation
                var distance = 5;

                // Get the error dimensions
                var width = jQuery(this).outerWidth();

                // Calculate starting position
                var start = width + distance;

                // Set the initial CSS
                jQuery(this).show().css({
                    display: 'block',
                    opacity: 0
                })
                // Animate the error message
                .animate({
                    opacity: 1
                }, 'slow');

            });



        }else {
            //$formId.submit();
            validateCaptchahome1('verify_captcha_home_form2.php');

        }
        // Prevent form submission
            e.preventDefault();
    });

    // Fade out error message when input field gains focus
    jQuery('.required').keypress(function(){
        var $parent = jQuery(this).parent();
        $parent.removeClass('error');
        jQuery('span.error',$parent).fadeOut();
    });

    jQuery('.captcha-image-home1').focus(function(){
        var $parent = jQuery(this).parent();
        $parent.removeClass('error');
        jQuery('span.error',$parent).fadeOut();
    });

    $("#ctime").change(function(){
        var $parent = $(this).parent();
        $parent.removeClass('error');
        $('span.error',$parent).fadeOut();
    });

    jQuery('#captcha_div_home1').on('click','a.captcha-image-home1',function(){
            var $parent = jQuery(this).parent();
            $parent.removeClass('error');
            jQuery('span.error',$parent).fadeOut();
            jQuery("#msghome1").fadeOut();
    });


});

//--------------------------------- Captcha Validation -------------------//


function validateCaptchahome1(argUrl)
{
    //alert(argUrl);
    var url=argUrl;
    var captchavalue1=document.homeform1.captcha_hid_home1.value;
    //alert(captchavalue1);
    if(captchavalue1==''){
        jQuery("#msghome1").text('The shape you selected is incorrect, please select the right one.');
        jQuery('#btn-submit111').show();
        jQuery('.wait-contactform-contact1').hide();
        jQuery("#msghome1").addClass('newerror')
        jQuery("#msghome1").show();return false;
    }
     jQuery("#msghome1").hide();
jQuery.ajax({
                type: "GET",
                url: url,
                data: "captchaValhome1="+captchavalue1,
                success: function(data){


                //alert(data);
                 if(data=="yes")
                 {

                    document.homeform1.submit();
                 }
                 else
                 {
                   jQuery("#msghome1").text('Incorrect answer, please select the right shape.');
                    jQuery('#btn-submit111').show();
                    jQuery('.wait-contactform-contact1').hide();
                   jQuery("#msghome1").addClass('newerror');
                   jQuery("#msghome1").show();
                   //alert("Incorrect answer, please select the right shape.");
                   refreshhome1();
                   return false;
                 }

            }
      });
      return false;
}


//--------------------------------- Save Captcha Function-------------------//
function save_captcha_home1(obj,arg)
{
_link=document.links;

for(i=0;i<_link.length;i++)
{
    if(_link[i].className=="captcha-image-home1") {
        if(_link[i]!=obj) {
            _link[i].childNodes[0].style.border='1px solid #C3C3C3';
        } else {
            _link[i].childNodes[0].style.border='1px solid #2588C9';
        }
    }
}
document.getElementById("captcha_hid_home1").value=arg;
}


function refreshhome1()
{
jQuery("#captcha_div_home1").load('captcha_code_home_form2.php?id=4');
}

var testimonials = tns({
  container: '.testimonials',
  autoplay: true,
  controls:false,
  nav:false,
  autoplayTimeout:8000
  // autoplayButton: {'style':'display:none'}
});

const header = document.querySelector('.header');

const headroom = new Headroom(header);
headroom.init();

</script>
    <style type="text/css">
        .popover {
            z-index: 11111111;
        }

    </style>
</body>

</html>
