<?php include_once( "config.php"); ?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <title>Acanac Inc.</title>
        <link rel="icon" type="image/x-icon" href="img/favicon.ico" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
<!-- Global site tag (gtag.js) - Google Ads: 1069262463 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-1069262463"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-1069262463');
</script>

<!-- Event snippet for Call Back Request conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1069262463/ybZHCLOykp8BEP_M7v0D'});
</script>
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5624568"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>
    </head>
    <body>
    	 <div class="first-bx" style="height: auto!important;">
            <div class="padd-box header">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="logo">
                            <a href="" title="Acanac Inc."><img src="img/logo.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <p>Call now: <a class="number btn" href="tel:1-855-235-8886">1-855-235-8886</a>
                               
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="thank-you">
        	<img src="img/thankyou.jpg">
        	<p><span>FOR CONTACTING US!</span> <br>We'll be in touch shortly.
</p>
        </div>

        <footer class="footer padd-box cf">
            <div class="f-btm cf">
                <p>©2019 Acanac Inc. All Rights Reserved.</p>
                <p>*You may use your own compatible modem with our internet plans. Free installation and activation.  Shipping fees may apply.  If you are not 100% satisfied with your purchase of Acanac High Speed internet service, you may cancel the service within 30 days of activation. Plans and pricing subject to technical availability. Full terms and conditions are available at <a href="https://www.acanac.com/">Acanac.com</a>.
</p>
            </div>
        </footer>
    </body>
</html>